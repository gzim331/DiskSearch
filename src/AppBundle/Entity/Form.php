<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Form
 *
 * @ORM\Table(name="form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormRepository")
 */
class Form
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="album", type="string", length=255)
     * @Assert\NotBlank(message="This field should not be blank")
     */
    private $album;

    /**
     * @var string
     *
     * @ORM\Column(name="performer", type="string", length=255)
     * @Assert\NotBlank(message="This field should not be blank")
     */
    private $performer;

    /**
     * @var int
     *
     * @ORM\Column(name="releasedata", type="integer")
     */
    private $releasedata;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     * @Assert\NotBlank(message="This field should not be blank")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="studio", type="string", length=255)
     */
    private $studio;

    /**
     * @var text
     *
     * @ORM\Column(name="tracks", type="text")
     */
    private $tracks;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set album
     *
     * @param string $album
     *
     * @return Form
     */
    public function setAlbum($album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return string
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Set performer
     *
     * @param string $performer
     *
     * @return Form
     */
    public function setPerformer($performer)
    {
        $this->performer = $performer;

        return $this;
    }

    /**
     * Get performer
     *
     * @return string
     */
    public function getPerformer()
    {
        return $this->performer;
    }

    /**
     * Set releasedata
     *
     * @param \DateTime $releasedata
     *
     * @return Form
     */
    public function setReleasedata($releasedata)
    {
        $this->releasedata = $releasedata;

        return $this;
    }

    /**
     * Get releasedata
     *
     * @return \DateTime
     */
    public function getReleasedata()
    {
        return $this->releasedata;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Form
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set studio
     *
     * @param string $studio
     *
     * @return Form
     */
    public function setStudio($studio)
    {
        $this->studio = $studio;

        return $this;
    }

    /**
     * Get studio
     *
     * @return string
     */
    public function getStudio()
    {
        return $this->studio;
    }

    /**
     * Set tracks
     *
     * @param string $tracks
     *
     * @return Form
     */
    public function setTracks($tracks)
    {
        $this->tracks = $tracks;

        return $this;
    }

    /**
     * Get tracks
     *
     * @return string
     */
    public function getTracks()
    {
        return $this->tracks;
    }
}

